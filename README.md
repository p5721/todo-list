Spreadsheet for remembering and coordinating tasks and issues
===================

Set up for engineering projects, the spreadsheet allows you to detail an item and delegate items among team members.  This project arises from a need for peers across firms to more effectively plan and remember together.  Also sometimes referred to as a `rolling action item list`.

The person organizing the work effort can use spreadsheet to keep track of actions that come up or issues that need solving and team members can add additional information an progress notes to each action or issue.  

This is part of the [pmcollabbc](https://gitlab.com/p5721) collection of spreadsheet tools.  

## Table of Contents

* [Usage](#usage) 
* [Installation](#installation) 
* [Uses](#uses) 
* [Needs](#needs) 
* [Contributing](#contributing) 
* [Credits](#credits) 
* [License](#license) 
* [Donations](#donations)


## Usage

This project involves using a Microsoft Excel spreadsheet.  The team can also use the sheet to share progress assessments for each item or issue.  

Rough guide included: 
`inf-scdesigns-action_list_procedure-R002.docx`

Spreadsheet tool:
`reg-scdesigns-action_issues_LOG_todo-TEMPLATE-R6-WBS.xlsm`

Each element of the file lives on its own tab, and the look-up tables for the work breakdown lives on its in own tab.  

1. Enter the Work Breakdown on the WBS sheet

2. Enter the Header information on the TODO list sheet and the ISSUES LOG

3. Enter the items into the relevant list

4. Sort routine will account for date and priority

5. Enter the status for each sheet


## Installation 

1. Download the repository to where you intend to work  
2. Extract the project into the folder  


## Needs

None.

## Requirements

 * Microsoft Excel and Microsoft Word (still need to test with other programs, likely to work with LibreOffice)

<!--
#### Debian
```bash
sudo apt install python3-feedparser
```

#### Fedora
```bash
sudo dnf install pandoc texlive-collection-context
```

#### Arch
```bash
sudo pacman -S pandoc texlive-core
```
-->
## Troubleshooting


## Contributing

Please do.  

## Credits

* Scott Cameron, ASTTBC

## License

Creative Commons Share and Share Alike, latest.

## Donations

TODO
<!--
We accept donations at the [VECTOR website](https://vectorradio.ca/donate/)
-->
